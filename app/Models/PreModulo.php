<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreModulo extends Model
{
    use HasFactory;

    protected $fillable = [
        'aluno_id',
        'modulo_id'
    ];

    public function modulos(){
        return $this->belongsTo('App\Models\Modulo', 'modulo_id');
    }
}
