<html>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Mazza</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<link rel="stylesheet" href="{{ asset('css/mystyle.css') }}">
<link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">

<title>Aulas</title>

<body>

    <div class="container area-header">

        <!-- INICIO DO NAVBAR -->
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('img/mazza.png') }}" alt="logo" style="width:80px;">
            </a>
            <div class="hv"></div>
            <h2 style="margin: 5px;">Aceleradora<br>de Negócios</h2>

            <div class="collapse navbar-collapse flex-row-reverse" id="navbarSupportedContent">

            </div>
        </nav>
        <!-- FIM DO NAVBAR -->
    </div>

    <!-- div dos textos -->
    <div class="aulas-textos">
        <div class="container">
            <h4 class="h4">PERFIL</h4>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <img class="rounded-circle" src="{{ asset('upload/image/'.Auth::user()->profile_photo_path) }}"
                        alt="{{ Auth::user()->name }}" widht="250px" height="250px" />
                </div>
                <div class="col-md-8">
                    <h1>{{ Auth::user()->name }}</h1>
                    <h5 class="h5">{{ date('Y', strtotime(Auth::user()->birthday)); }},
                        {{ date('d M', strtotime(Auth::user()->birthday)); }}</h5>
                    <h5 class="h5">{{ Auth::user()->city }}, {{ Auth::user()->country }}</h5>
                    <h5 class="h5">Nível {{ $nivel }}</h5>
                    <h5 class="h5">Membro desde {{ date('d M Y', strtotime(Auth::user()->created_at)); }}</h5>
                    <h5 class="h5">Interesses:
                        @foreach($interesses as $key => $interesse)
                        <b>{{ $interesse->modulos->nome }}</b>,
                        @endforeach
                    </h5>
                </div>
            </div>
        </div>
        <div class="container mt-5">
            <div class="progress tamanhoProgress">
                <div class="progress-bar progress1 active" role="progressbar" style="{{$size}}"></div>
                <div class="progress-bar progress2" role="progressbar" style="{{$sizediff}}"></div>
            </div>
            <h6 class="h6Prog"><b class="progPadd">|</b><b class="progPadd">|</b><b class="progPadd">|</b><b class="progPadd">|</b><b class="progPadd">|</b><b>|</b></h6>
            <h6 class="h6Prog"><b class="bolderProg">Iniciante</b><b class="bolderProg3">Básico</b><b class="bolderProg2">Intermédio</b><b class="bolderProg4">Profissional</b><b class="bolderProg2">Experiente</b><b>Professor</b></h6>
        </div>
        <div class="container mt-5">
            <h4 class="h4">ADESÃO E FACTURAÇÃO</h4>
            <hr>
            <div class="row">
                <div style="margin: 20px;">
                    E-MAIL
                    <br>
                    PALAVRA-PASSE
                    <br>
                    TELEFONE
                    <br>
                    Método de Pagamento
                </div>
                <div class="hvCard"></div>
                <div style="margin: 20px;">
                    {{ Auth::user()->email }} <label class="badge badge-primary">Alterar</label>
                    <br>
                    ******** <label class="badge badge-primary">Alterar</label>
                    <br>
                    {{ Auth::user()->phone }} <label class="badge badge-primary">Alterar</label>
                    <br>
                    <?php $l = substr($card->numero, -4); $n = '•••• •••• •••• '.$l; ?>
                    {{ $card->rede }} {{ $n }} <label class="badge badge-primary">Alterar</label>
                </div>
            </div>
        </div>
        <div class="container mt-5">
            <h4 class="h4">DETALHES DO PLANO</h4>
            <hr>
            <div class="row">
                <div style="margin: 20px;">
                    Periodicidade
                    <br>
                    Término do plano
                </div>
                <div class="hvPlan"></div>
                <div style="margin: 20px;">
                    {{ $plano->planos->periodicidade }} <label class="badge badge-primary">Alterar</label>
                    <br>
                    {{ date('d M Y', strtotime($plano->termino_plano));  }}
                </div>
            </div>
        </div>
        <hr>
        <div class="container mt-5">
            <h6 class="h6Cont">Contactos:&nbsp;&nbsp;<i class="far fa-envelope"></i>&nbsp;info@believer.co.mz
                &nbsp;&nbsp;&nbsp;<i class="fas fa-phone-square-alt"></i>&nbsp;<i class="fab fa-whatsapp"></i>&nbsp;+258
                86 97 50000 &nbsp;&nbsp;&nbsp;<i class="fas fa-map-marker-alt"></i>&nbsp;Rua do Brado Africano, Nº46
            </h6>
        </div>
    </div>

    <!-- <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script> -->

</body>

</html>