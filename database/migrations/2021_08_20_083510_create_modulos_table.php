<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modulos', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('slug');
            $table->enum('formacao', ['Digital', 'Presencial'])->default('Digital');
            $table->foreignId('mentor_id')->references('id')->on('mentors')->onDelete('cascade')->onUpdate('cascade');
            $table->string('price')->default('0');
            $table->string('image')->nullable();
            $table->enum('nivel', ['Iniciante', 'Básico', 'Intermédio', 'Profissional', 'Experiente', 'Professor'])->default('Iniciante');
            $table->foreignId('categoria_id')->references('id')->on('categorias')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modulos');
    }
}
