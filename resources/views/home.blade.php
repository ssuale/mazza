@extends('template.master')
@section('admin')


<section style="padding-top:50px;  background-color: #fbfbfb;">
 <div class="container" style="padding-top:30px" >
    <div class="row">
      <div class="col-sm-12 col-md-3 col-image-author">
        <div class="image-author">
        <img src="{{ asset('images/author-11.jpg') }}" style="width: 100%; height: 100%;"> 
        </div>
      </div>
      <div class="col-md-9 col-info-author">
        <div class="info-author">
          <p >Steve Jobs</p>
          <i >Coach</i>
          <div class="line-div"></div>
          <strong>
          Believer Team Leader é uma empresa moçambicana que
            atua na área de consultoria e apoio ao negócio por meio da
            Educação.
          
          </strong>
          <br>
        
          <span>
          Oferecemos ferramentas apropriadas para cada fase e tipo de negócio
          através dos nossos programas, servindo como uma força de
          disseminação das boas práticas de gestão e negócios para líderes.
          </span>
        </div>
      </div>
    </div> 
    
    <!-- Courses -->
    <div class="row" style="margin-top: 40px;">
      <div class="col-md-3">
        <div class="wrapper-course" >
          <div class="course-icon">
            <img src="{{ asset('images/courses-01.jpg') }}" alt="..." style="width: 100%; height: 100%;">
          </div>
          <a href="#">
        Abertura de uma 
        Média empresa
       </a>
      <div class="row" style="margin-top: 15px;">
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-bell" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-share" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        </div>
      </div>
      
    </div>
    <div class="col-md-3">
        <div class="wrapper-course" >
          <div class="course-icon">
            <img src="{{ asset('images/courses-03.jpg') }}" alt="..." style="width: 100%; height: 100%;">
          </div>
          <a href="/course-details">
       Economia
       </a>
      <div class="row" style="margin-top: 15px;">
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-bell" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-share" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        </div>
      </div>
      
    </div> <div class="col-md-3">
        <div class="wrapper-course" >
          <div class="course-icon">
            <img src="{{ asset('images/courses-02.jpg') }}" alt="..." style="width: 100%; height: 100%;">
          </div>
          <a href="#">
        Marketing Digital
       </a>
      <div class="row" style="margin-top: 15px;">
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-bell" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-share" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        </div>
      </div>
      
    </div> <div class="col-md-3">
        <div class="wrapper-course" >
          <div class="course-icon">
            <img src="{{ asset('images/courses-05.jpg') }}" alt="..." style="width: 100%; height: 100%;">
          </div>
          <a href="/course-details">
        Business
       </a>
      <div class="row" style="margin-top: 15px;">
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-bell" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-share" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        </div>
      </div>
      
    </div>
    </div>
    
 </div>
 <div class="container" style="padding-top:30px" >
    <div class="row">
      <div class="col-sm-12 col-md-3 col-image-author">
        <div class="image-author">
        <img src="{{ asset('images/teach2.jpg') }}" style="width: 100%; height: 100%;"> 
        </div>
      </div>
      <div class="col-md-9 col-info-author">
        <div class="info-author">
          <p >Pamela Foster</p>
          <i >Designer</i>
          <div class="line-div"></div>
          <strong>
          Believer Team Leader é uma empresa moçambicana que
            atua na área de consultoria e apoio ao negócio por meio da
            Educação.
          
          </strong>
          <br>
        
          <span>
          Oferecemos ferramentas apropriadas para cada fase e tipo de negócio
          através dos nossos programas, servindo como uma força de
          disseminação das boas práticas de gestão e negócios para líderes.
          </span>
        </div>
      </div>
    </div> 
    
    <!-- Courses -->
    <div class="row" style="margin-top: 40px;">
      <div class="col-md-3">
        <div class="wrapper-course" >
          <div class="course-icon">
            <img src="{{ asset('images/courses-04.jpg') }}" alt="..." style="width: 100%; height: 100%;">
          </div>
          <a href="/course-details">
        Abertura de uma 
        Média empresa
       </a>
      <div class="row" style="margin-top: 15px;">
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-bell" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-share" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        </div>
      </div>
      
    </div>
    <div class="col-md-3">
        <div class="wrapper-course" >
          <div class="course-icon">
            <img src="{{ asset('images/courses-03.jpg') }}" alt="..." style="width: 100%; height: 100%;">
          </div>
          <a href="/course-details">
       Economia
       </a>
      <div class="row" style="margin-top: 15px;">
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-bell" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-share" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        </div>
      </div>
      
    </div> <div class="col-md-3">
        <div class="wrapper-course" >
          <div class="course-icon">
            <img src="{{ asset('images/courses-02.jpg') }}" alt="..." style="width: 100%; height: 100%;">
          </div>
          <a href="#">
        Marketing Digital
       </a>
      <div class="row" style="margin-top: 15px;">
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-bell" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-share" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        </div>
      </div>
      
    </div> <div class="col-md-3">
        <div class="wrapper-course" >
          <div class="course-icon">
            <img src="{{ asset('images/courses-01.jpg') }}" alt="..." style="width: 100%; height: 100%;">
          </div>
          <a href="/course-details">
        Business
       </a>
      <div class="row" style="margin-top: 15px;">
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-bell" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-share" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        </div>
      </div>
      
    </div>
    </div>
    
 </div> <div class="container" style="padding-top:30px" >
    <div class="row">
      <div class="col-sm-12 col-md-3 col-image-author">
        <div class="image-author">
         <img src="{{ asset('images/teach3.jpg') }}" style="width: 100%; height: 100%;"> 
        </div>
      </div>
      <div class="col-md-9 col-info-author">
        <div class="info-author">
          <p >Katerina Holmes</p>
          <i >Gestor</i>
          <div class="line-div"></div>
          <strong>
          Believer Team Leader é uma empresa moçambicana que
            atua na área de consultoria e apoio ao negócio por meio da
            Educação.
          
          </strong>
          <br>
        
          <span>
          Oferecemos ferramentas apropriadas para cada fase e tipo de negócio
          através dos nossos programas, servindo como uma força de
          disseminação das boas práticas de gestão e negócios para líderes.
          </span>
        </div>
      </div>
    </div> 
    
    <!-- Courses -->
    <div class="row" style="margin-top: 40px;">
      <div class="col-md-3">
        <div class="wrapper-course" >
          <div class="course-icon">
            <img src="{{ asset('images/courses-04.jpg') }}" alt="..." style="width: 100%; height: 100%;">
          </div>
          <a href="/course-details">
        Abertura de uma 
        Média empresa
       </a>
      <div class="row" style="margin-top: 15px;">
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-bell" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-share" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        </div>
      </div>
      
    </div>
    <div class="col-md-3">
        <div class="wrapper-course" >
          <div class="course-icon">
            <img src="{{ asset('images/courses-05.jpg') }}" alt="..." style="width: 100%; height: 100%;">
          </div>
          <a href="#">
       Economia
       </a>
      <div class="row" style="margin-top: 15px;">
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-bell" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-share" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        </div>
      </div>
      
    </div> <div class="col-md-3">
        <div class="wrapper-course" >
          <div class="course-icon">
            <img src="{{ asset('images/courses-02.jpg') }}" alt="..." style="width: 100%; height: 100%;">
          </div>
          <a href="#">
        Marketing Digital
       </a>
      <div class="row" style="margin-top: 15px;">
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-bell" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-share" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        </div>
      </div>
      
    </div> <div class="col-md-3">
        <div class="wrapper-course" >
          <div class="course-icon">
            <img src="{{ asset('images/courses-04.jpg') }}" alt="..." style="width: 100%; height: 100%;">
          </div>
          <a href="#">
        Business
       </a>
      <div class="row" style="margin-top: 15px;">
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-bell" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        <div class="col-3">
          <i class="fa fa-share" aria-hidden="true"></i>
          <span>2.5K</span>
        </div>
        </div>
      </div>
      
    </div>
    </div>
    
 </div>

 <!-- Teachers -->


<div class="container" style="padding-top:80px">
  <div class="row">
    <div class="col-md-6" style="padding-top:30px">
      <div class="row">
        <div class="col-md-6 col-image-author">
            <div class="image-author">
              <img src="{{ asset('images/teach3.jpg') }}" style="width: 100%; height: 100%;"> 
            </div>
        </div>
      <div class="col-md-6 col-info-author">
          <div class="info-author">
              <p >Katerina Holmes</p>
              <i >Gestor</i>
                
          </div>
      </div>
        
      </div>
    </div>
    <div class="col-md-6" style="padding-top:30px">
      <div class="row">
        <div class="col-md-6 col-image-author">
            <div class="image-author">
              <img src="{{ asset('images/teach2.jpg') }}" style="width: 100%; height: 100%;"> 
            </div>
        </div>
      <div class="col-md-6 col-info-author">
          <div class="info-author">
              <p >Joh Tivene</p>
              <i >CEO</i>
                
          </div>
      </div>
        
      </div>
    </div><div class="col-md-6" style="padding-top:30px">
      <div class="row">
        <div class="col-md-6 col-image-author">
            <div class="image-author">
              <img src="{{ asset('images/teach4.jpg') }}" style="width: 100%; height: 100%;"> 
            </div>
        </div>
      <div class="col-md-6 col-info-author">
          <div class="info-author">
              <p >Carlos Kally</p>
              <i >Programdor</i>
                
          </div>
      </div>
        
      </div>
    </div><div class="col-md-6" style="padding-top:30px">
      <div class="row">
        <div class="col-md-6 col-image-author">
            <div class="image-author">
              <img src="{{ asset('images/author-11.jpg') }}" style="width: 100%; height: 100%;"> 
            </div>
        </div>
      <div class="col-md-6 col-info-author">
          <div class="info-author">
              <p >Steve Holmes</p>
              <i >Designer</i>
                
          </div>
      </div>
        
      </div>
    </div>
  </div>
</div>
</section>
@endsection