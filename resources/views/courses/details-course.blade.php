@extends('template.master')
@section('admin')


<section style="padding-top:50px;">
<div class="container"  >
    <div class="row">
        <div class="col-md-8"> 
            <div class="course-image">
                <img src="{{ asset('images/courses-details.jpg') }}" style="width: 100%;">
            </div>
            <p class="title-course">Finance & Investment Series: Learn to Budget and Calculate Your Net Worth.</p>
            <div class="author-thumb">
            <img class="author-thumb-img"src="{{ asset('images/author-11.jpg') }}" alt="Author">
             <span> | Katerina Holmes</span> 
            </div>
            <h2 >Description:</h2>
            <p class="text-course">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown 
                printer took a galley of type and scrambled it to make a type specimen book.</p>
                <p class="text-course">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
        </div>


        <div class="col-md-4">
            <div class="col-payCourse">
             <center><p>500,00 MT </p></center>
             <hr>
             <span class="span-left"> Language:</span> <span class="span-right"> Portugues</span>
             <hr>
            
             <span class="span-left">Duration:</span> <span class="span-right"> 08 hr 15 mins</span>
             <hr>
         
             <span class="span-left">Categoria:</span> <span class="span-right"> Financia</span>
             <hr>
             <br>
             <center><a href="#" class="btn btn-dark">Comprar Agora</a></center>
            </div>
        </div>
    </div>
</div>
</section>
@endsection