<html>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Mazza</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<link rel="stylesheet" href="{{ asset('css/credit.css') }}">
<link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">
<script src="{{ asset('/js/credit.js') }}"></script>

<link rel="stylesheet" href="{{ asset('css/mystyle.css') }}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/3.0.0/jquery.payment.min.js"></script>

<title>Aulas</title>

<body>

    <div class="container area-header">

        <!-- INICIO DO NAVBAR -->
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="#">
                <h4>Tenha conhecimento de um Mazza</h4>
            </a>

            <div class="collapse navbar-collapse flex-row-reverse" id="navbarSupportedContent">
                <img src="{{ asset('img/mazza.png') }}" alt="logo" style="width:80px;">
            </div>
        </nav>
        <!-- FIM DO NAVBAR -->
    </div>

    <!-- div dos textos -->
    <div class="aulas-textos">
        <div class="container">
            <p class="p2">Confirmação de Pagamento</p>
            <hr class="hr">
        </div>
        <!--fim da div dos textos -->

        <!-- CHECKBOXS -->
        <div class="container checkpartes-alldivs">
            <div class="container-fluid">
                <form method="post" action="{{ route('step7') }}" id="form">
                    @csrf
                    <input type="hidden" name="aluno_id" value="{{ $input['aluno_id'] }}" required>
                    <div class="row">
                        <div class="col-md-2 div-botao">
                            <button type="buttom" class="btn-primary botao" disabled>
                                <i class="fas fa-angle-left"></i>
                            </button>
                        </div>

                        <div class="col-md-8">
                            <div>
                                <div class="card-body" style="height: 350px">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="cc-number" class="control-label">Cartão de
                                                    crédito ou débito</label>
                                                <?php 
                                                $f = substr($input['numero'], 0, 1); 
                                                $l = substr($input['numero'], -3); 
                                                $n = $f.'***'.$l;
                                                ?>
                                                <h5>{{ $n }}</h5>
                                                <input type="hidden" name="numero" value="{{ $input['numero'] }}"
                                                    required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="rede" class="form-label">Rede</label>
                                                <h5>{{ $input['rede'] }}</h5>
                                                <input type="hidden" name="rede" value="{{ $input['rede'] }}" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="numeric" class="control-label">Nome</label>
                                        <h5>{{ $input['nome'] }}</h5>
                                        <input type="hidden" name="nome" value="{{ $input['nome'] }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="numeric" class="control-label">Apelido</label>
                                        <h5>{{ $input['apelido'] }}</h5>
                                        <input type="hidden" name="apelido" value="{{ $input['apelido'] }}" required>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="cc-exp" class="control-label">Data que expira (mês/ano
                                                    )</label>
                                                <h5>{{ $input['data_expira'] }}</h5>
                                                <input type="hidden" name="data_expira"
                                                    value="{{ $input['data_expira'] }}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="cc-cvc" class="control-label">Código de segurança</label>
                                                <h5>{{ $input['cvv'] }}</h5>
                                                <input type="hidden" name="cvv" value="{{ $input['cvv'] }}" required>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8 mt-5"
                                    style="display: flex; justify-content: center; align-items: center; border-radius: 50px; width: 480px; align: center;">
                                    <input type="hidden" name="total" value="{{ $plano->id }}">
                                    <h1 class="text-center"><b>MT {{number_format($plano->taxa, 2)}}/mês</b></h12>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <hr class="hr">
                            <div class="row mt-4">
                                <div class="col-md-2"></div>
                                <div class="col-md-8"
                                    style="display: flex; justify-content: center; align-items: center; border-radius: 50px; width: 480px; align: center;">
                                    <a href="#" class="m-2">Voltar</a> |
                                    <button type="submit"
                                        class="btn px-4 btn-estilo rounded-pill m-2">Confirmar</button>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </div>

                        <div class=" col-md-2"> </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END CHECKBOXS -->
    </div>

    <!-- <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script> -->

</body>

</html>