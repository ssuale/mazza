<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Solicitacao;

class SolicitacaoController extends Controller
{
    //
    public function storeIndividual(Request $request){
        $request->validate([
            'nome_completo' => 'required',
            'email' => 'required',
            'pais' => 'required',
            'provincia' => 'required',
            'cidade' => 'required',
            'descricao' => 'required',
        ]);

        $input = $request->all();

        $solicitacao = Solicitacao::create($input);

        return redirect()->route('modulos', $solicitacao->id);
    }

    public function storeEmpresa(Request $request){
        $request->validate([
            'nome_completo' => 'required',
            'email' => 'required',
            'pais' => 'required',
            'provincia' => 'required',
            'cidade' => 'required',
            'descricao' => 'required',
        ]);
        
        $input = $request->all();

        $solicitacao = Solicitacao::create($input);

        return redirect()->route('modulos', $solicitacao->id);
    }

    public function enviarSolicitacao(Request $request){
        $id = $request->solicitacao_id;
        $solicitacao = Solicitacao::find($id);
        $modulos = array();

        foreach($request->modulo_id as $key => $modulo){
            $modulos[] =  $request->modulo_id;
        }
        // $solicitacao->tipo
        // $solicitacao->nome_completo
        // $solicitacao->nuit
        // $solicitacao->profissao
        // $solicitacao->email
        // $solicitacao->telefone
        // $solicitacao->pais
        // $solicitacao->provincia
        // $solicitacao->cidade
        // $solicitacao->endereco
        // $solicitacao->nr_pessoa
        // $solicitacao->descricao
        // foreach($modulos as $key => $mod){
        //     echo $mod.',';
        // }

        return redirect()->route('index');
    }
}
