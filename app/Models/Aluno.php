<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'fullname'
    ];

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function cards(){
        return $this->hasMany('App\Models\CardInfo');
    }

    public function modulos(){
        return $this->belongsToMany('App\Models\Modulo');
    }

    public function subscricao(){
        return $this->belongsToMany('App\Models\Subscricao');
    }
}
