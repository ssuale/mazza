<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Models\Modulo;
use App\Models\Aula;
use App\Models\Aluno;
use App\Models\PreModulo;
use App\Models\CardInfo;
use App\Models\Solicitacao;
use App\Models\PlanoSubscricao;
use App\Models\Subscricao;
use App\Models\ModuloAssinados;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Illuminate\Http\Request;


class StepController extends Controller
{
    public function step2(Request $request){

        $email = $request->email;
    	return view('signin',compact('email'));
    }


    public function step3(Request $request){

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'senha' => 'required',
          ]);
        

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->senha),
        ]);

        if($user){
            $student = Aluno::create([
                'user_id' => $user->id,
                'fullname' => $request->name,
            ]);
            
            return redirect()->route('step4',$user->id);
        }else{
            return redirect()->back();
        }
       
    }


    public function step4($id){
        $student = Aluno::where('user_id', $id)->first();
        $modulos = Modulo::all();
        
        return view('aulas', compact('modulos', 'student'));
    }

    public function step5(Request $request){
        $request->validate([
            'aluno_id' => 'required',
            'modulo_id' => 'required',
        ]);

        $student = $request->aluno_id;
        $planos = PlanoSubscricao::all();
        $sumPrice = 0;

        foreach($request->modulo_id as $key => $modulo){
            $preModulo = PreModulo::create([
                'aluno_id' => $request->aluno_id,
                'modulo_id' => $request->modulo_id[$key],
            ]);
        }

        foreach($request->modulo_id as $key => $modulo){
            $mod = PreModulo::where('modulo_id', $request->modulo_id[$key])
                            ->join('modulos', 'pre_modulos.modulo_id', '=', 'modulos.id')
                            ->first();
            
            $sumPrice += $mod->price;
        }

        return view('pagamento', compact('student', 'sumPrice', 'planos'));
    }

    public function step6(Request $request){
        $total = $request->total;

        $plano = PlanoSubscricao::where('id', $total)->first();
        $input = $request->all();

        return view('conf_pagamento', compact('input', 'plano'));
    }

    public function step7(Request $request){
        $request->validate([
            'numero' => 'required',
            'rede' => 'required',
            'nome' => 'required',
            'apelido' => 'required',
            'data_expira' => 'required',
            'cvv' => 'required',
            'aluno_id' => 'required',
        ]);

        $input = [
            'numero' => $request->numero,
            'rede' => $request->rede,
            'nome' => $request->nome,
            'apelido' => $request->apelido,
            'data_expira' => $request->data_expira,
            'cvv' => $request->cvv,
            'aluno_id' => $request->aluno_id,
        ];

        $plano = PlanoSubscricao::where('id', $request->total)->first();

        if($plano->periodicidade == "Mensal"){
            $data = date('Y-m-d', strtotime('1 month'));
        }else if($plano->periodicidade == "Trimestral"){
            $data = date('Y-m-d', strtotime('3 month'));
        }else if($plano->periodicidade == "Semestral"){
            $data = date('Y-m-d', strtotime('6 month'));
        }else if($plano->periodicidade == "Anual"){
            $data = date('Y-m-d', strtotime('12 month'));
        }

        $input2 = [
            'aluno_id' => $request->aluno_id,
            'plano_subscricao_id' => $request->total,
            'termino_plano' => $data,
        ];

        $card = CardInfo::create($input);

        $subscricao = Subscricao::create($input2);

        return redirect()->route('index');
    }

    public function solicitar(){

        return view('solicitacao');
    }

    public function modulo($id){
        $solicitacao = Solicitacao::find($id);
        $modulos = Modulo::all();
        
        return view('modulos', compact('solicitacao', 'modulos'));
    }

    public function aulasAssistir(){
        $aluno = Aluno::where('user_id', auth()->user()->id)->first();
        $modulo = ModuloAssinados::where('aluno_id', $aluno->id)->first();
        $aulas = Aula::where('modulo_id', $modulo->id)->get();

        return view('aulas_assistir', compact('aulas'));
    }

}