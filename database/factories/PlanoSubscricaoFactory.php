<?php

namespace Database\Factories;

use App\Models\PlanoSubscricao;
use Illuminate\Database\Eloquent\Factories\Factory;

class PlanoSubscricaoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PlanoSubscricao::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
