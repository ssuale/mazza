<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanoSubscricaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plano_subscricaos', function (Blueprint $table) {
            $table->id();
            $table->enum('periodicidade', ['Mensal', 'Trimestral', 'Semestral', 'Anual'])->default('Mensal');
            $table->float('taxa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plano_subscricaos');
    }
}
