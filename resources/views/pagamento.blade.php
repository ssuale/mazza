<html>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Mazza</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<link rel="stylesheet" href="{{ asset('css/credit.css') }}">
<link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">
<script src="{{ asset('/js/credit.js') }}"></script>

<link rel="stylesheet" href="{{ asset('css/mystyle.css') }}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/3.0.0/jquery.payment.min.js"></script>

<title>Aulas</title>

<body>

    <div class="container area-header">

        <!-- INICIO DO NAVBAR -->
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="#">
                <h4>Tenha conhecimento de um Mazza</h4>
            </a>

            <div class="collapse navbar-collapse flex-row-reverse" id="navbarSupportedContent">
                <img src="{{ asset('img/mazza.png') }}" alt="logo" style="width:80px;">
            </div>
        </nav>
        <!-- FIM DO NAVBAR -->
    </div>

    <!-- div dos textos -->
    <div class="aulas-textos">
        <div class="container">
            <p class="p1">Concluiu com sucesso o passo #3</p>
            <p class="p2">Escolha a forma de pagamento</p>
            <hr class="hr">
        </div>
        <!--fim da div dos textos -->

        <!-- CHECKBOXS -->
        <div class="container checkpartes-alldivs">
            <div class="container-fluid">
                <form method="post" action="{{ route('step6') }}" id="form">
                    @csrf
                    <input type="hidden" name="aluno_id" value="{{ $student }}" required>
                    <div class="row">
                        <div class="col-md-2 div-botao">
                            <button type="buttom" class="btn-primary botao" disabled>
                                <i class="fas fa-angle-left"></i>
                            </button>
                        </div>

                        <div class="col-md-8">
                            <div>
                                <div class="card-body" style="height: 350px">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="cc-number" class="control-label">Cartão de
                                                    crédito ou débito</label>
                                                <input id="cc-number" type="tel" name="numero"
                                                    class="input-lg form-control cc-number" autocomplete="cc-number"
                                                    placeholder="•••• •••• •••• ••••" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="rede" class="form-label">Rede</label>
                                                <select name="rede" id="rede" class="form-control" required>
                                                    <option value="">Seleccione</option>
                                                    <option value="Visa">Visa</option>
                                                    <option value="Master Card">Master Card</option>
                                                    <option value="Discover">Discover</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="numeric" class="control-label">Nome</label>
                                        <input type="text" name="nome" class="input-lg form-control"
                                            placeholder="Escreva aqui.." required>
                                    </div>
                                    <div class="form-group">
                                        <label for="numeric" class="control-label">Apelido</label>
                                        <input type="text" name="apelido" class="input-lg form-control"
                                            placeholder="Escreva aqui.." required>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="cc-exp" class="control-label">Data que expira (mês/ano
                                                    )</label>
                                                <input id="cc-exp" type="tel" name="data_expira"
                                                    class="input-lg form-control cc-exp" autocomplete="cc-exp"
                                                    placeholder="•• / ••" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="cc-cvc" class="control-label">Código de segurança</label>
                                                <input id="cc-cvc" type="tel" name="cvv"
                                                    class="input-lg form-control cc-cvc" autocomplete="off"
                                                    placeholder="••••" required>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <select name="total" id="total" class="form-control mt-5" required>
                                        @foreach($planos as $key => $plano)
                                        <option value="{{ $plano->id}}">MT
                                            {{number_format($plano->taxa, 2)}}/{{ $plano->periodicidade }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                            <hr class="hr">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="gridCheck">
                                        <label class="form-check-label" for="gridCheck">
                                            <p style="text-align: justify; font-size: 12px;">Concordo com os termos de
                                                uso e
                                                não propagação de pirataria. A Mazza
                                                continuará a cobrar automaticamente a taxa mensal de subscrição em seu
                                                método de pagamento ate que você cancele.</span></p>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr class="hr">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8 text-center">
                                    <a href="{{ route('request') }}" class="btn px-4 btn-estilo rounded-pill">Solicito
                                        Cotação</a>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </div>

                        <div class="col-md-2 div-botao">
                            <button type="submit" class="btn-primary botao">
                                <i class="fas fa-angle-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END CHECKBOXS -->
    </div>

    <!-- <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script> -->

</body>

</html>