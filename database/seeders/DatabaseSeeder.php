<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Aula;
use App\Models\Categoria;
use App\Models\Mentor;
use App\Models\Modulo;
use App\Models\Pergunta;
use App\Models\PlanoSubscricao;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //
        $categorias = [
            ['category_name' => 'Digital'],
            ['category_name' => 'Presencial'],
            ['category_name' => 'Categoria 3'],
        ];

        foreach ($categorias as $categoria) {
            Categoria::create($categoria);
        }

        //
        $mentores = [
            ['nome' => 'Mentor 1'],
            ['nome' => 'Mentor 2'],
            ['nome' => 'Mentor 3']
        ];

        foreach ($mentores as $mentor) {
            Mentor::create($mentor);
        }

        //
        $modulos = [
            ['nome' => 'Modulo 1', 'slug' => 'modulo-1', 'formacao' => 'Digital', 'mentor_id' => 1, 'nivel' => 'Iniciante', 'categoria_id' => 1],
            ['nome' => 'Modulo 2', 'slug' => 'modulo-2', 'formacao' => 'Presencial', 'mentor_id' => 2, 'nivel' => 'Intermédio', 'categoria_id' => 1],
            ['nome' => 'Modulo 3', 'slug' => 'modulo-3', 'formacao' => 'Digital', 'mentor_id' => 2, 'nivel' => 'Professor', 'categoria_id' => 2],
            ['nome' => 'Modulo 4', 'slug' => 'modulo-4', 'formacao' => 'Presencial', 'mentor_id' => 1, 'nivel' => 'Professor', 'categoria_id' => 3],
            ['nome' => 'Modulo 5', 'slug' => 'modulo-5', 'formacao' => 'Presencial', 'mentor_id' => 3, 'nivel' => 'Iniciante', 'categoria_id' => 3],
            ['nome' => 'Modulo 6', 'slug' => 'modulo-6', 'formacao' => 'Digital', 'mentor_id' => 2, 'nivel' => 'Básico', 'categoria_id' => 2],
            ['nome' => 'Modulo 7', 'slug' => 'modulo-7', 'formacao' => 'Digital', 'mentor_id' => 1, 'nivel' => 'Iniciante', 'categoria_id' => 2],
            ['nome' => 'Modulo 8', 'slug' => 'modulo-8', 'formacao' => 'Digital', 'mentor_id' => 3, 'nivel' => 'Experiente', 'categoria_id' => 2],
            ['nome' => 'Modulo 9', 'slug' => 'modulo-9', 'formacao' => 'Presencial', 'mentor_id' => 3, 'nivel' => 'Experiente', 'categoria_id' => 3],
            ['nome' => 'Modulo 10', 'slug' => 'modulo-10', 'formacao' => 'Presencial', 'mentor_id' => 2, 'nivel' => 'Iniciante', 'categoria_id' => 1],
        ];

        foreach ($modulos as $modulo) {
            Modulo::create($modulo);
        }

        //
        $perguntas = [
            ['title' => 'Pergunta 1', 'description' => 'Believer Team Leader é uma empresa moçambicana que atua na área de consultoria e apoio ao negócio por meio da Educação.'],
            ['title' => 'Pergunta 2', 'description' => 'Believer Team Leader é uma empresa moçambicana que atua na área de consultoria e apoio ao negócio por meio da Educação.'],
            ['title' => 'Pergunta 3', 'description' => 'Believer Team Leader é uma empresa moçambicana que atua na área de consultoria e apoio ao negócio por meio da Educação.'],
            ['title' => 'Pergunta 4', 'description' => 'Believer Team Leader é uma empresa moçambicana que atua na área de consultoria e apoio ao negócio por meio da Educação.'],
            ['title' => 'Pergunta 5', 'description' => 'Believer Team Leader é uma empresa moçambicana que atua na área de consultoria e apoio ao negócio por meio da Educação.'],
        ];

        foreach ($perguntas as $pergunta) {
            Pergunta::create($pergunta);
        }

        // 'Mensal', 'Trimestral', 'Semestral', 'Anual'
        $planos = [
            ['periodicidade' => 'Mensal', 'taxa' => 450],
            ['periodicidade' => 'Trimestral', 'taxa' => 1300],
            ['periodicidade' => 'Semestral', 'taxa' => 2600],
            ['periodicidade' => 'Anual', 'taxa' => 5000],
        ];

        foreach ($planos as $plano) {
            PlanoSubscricao::create($plano);
        }

        //
        $aulas = [
            ['modulo_id' => 1, 'nome' => 'how we attend meetings at big tech companies', 'position' => 1, 'link' => 'https://www.youtube.com/embed/lpcpsCY4Mco'],
            ['modulo_id' => 1, 'nome' => 'how we write/review code in big tech companies', 'position' => 2, 'link' => 'https://www.youtube.com/embed/rR4n-0KYeKQ'],
            ['modulo_id' => 1, 'nome' => 'GitHub Copilot e IA vão acabar com seu EMPREGO?', 'position' => 3, 'link' => 'https://www.youtube.com/embed/xApA2RX2ipU'],
            ['modulo_id' => 1, 'nome' => 'Design UX/UI, Home Office, Dev em Cidade Pequena, Portfólio, Insegurança', 'position' => 4, 'link' => 'https://www.youtube.com/embed/RlK8nu_YErc'],
            ['modulo_id' => 1, 'nome' => 'A day in the life of a Google Software Engineer', 'position' => 5, 'link' => 'https://www.youtube.com/embed/a0glBQXOcl4'],
            ['modulo_id' => 1, 'nome' => '5 Years of Coding - Everything I have Learned', 'position' => 6, 'link' => 'https://www.youtube.com/embed/Zn_f6el0TKw']
        ];

        foreach ($aulas as $aula) {
            Aula::create($aula);
        }
    }
}
