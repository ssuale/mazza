<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscricao extends Model
{
    use HasFactory;

    protected $fillable = [
        'aluno_id',
        'plano_subscricao_id',
        'termino_plano'
    ];

    public function aluno(){
        return $this->belongsTo('App\Models\Aluno', 'aluno_id');
    }

    public function planos(){
        return $this->belongsTo('App\Models\PlanoSubscricao', 'plano_subscricao_id');
    }
}
