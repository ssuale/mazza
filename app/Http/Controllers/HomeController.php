<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pergunta;

class HomeController extends Controller
{
    public function index()
    {
        $perguntas = Pergunta::all();
        
        return view('index', compact('perguntas'));        
    }

    public function home()
    {
        return view('home');
        
    }
}
