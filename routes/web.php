<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\StepController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\DashController;
use App\Http\Controllers\SolicitacaoController;

// Basic Route
// Route::get('/', function(){
//     return view('index');
// });

Route::get('/', [HomeController::class, 'index'])->name('index');

Route::get('/step2', [StepController::class, 'step2'])->name('step2');
Route::post('/step3', [StepController::class, 'step3'])->name('step3');
Route::get('/step4/{id}', [StepController::class, 'step4'])->name('step4');
Route::post('/step5', [StepController::class, 'step5'])->name('step5');
Route::post('/step6', [StepController::class, 'step6'])->name('step6');
Route::post('/step7', [StepController::class, 'step7'])->name('step7');

Route::get('/request', [StepController::class, 'solicitar'])->name('request');
Route::get('/modulos/{id}', [StepController::class, 'modulo'])->name('modulos');

Route::get('/aulas', [StepController::class, 'aulasAssistir'])->name('aulas');

Route::post('/storeSolicitarIndividual', [SolicitacaoController::class, 'storeIndividual'])->name('storeSolicitarIndividual');
Route::post('/storeSolicitarEmpresa', [SolicitacaoController::class, 'storeEmpresa'])->name('storeSolicitarEmpresa');
Route::post('/sendRequest', [SolicitacaoController::class, 'enviarSolicitacao'])->name('sendRequest');

Route::get('/perfil', [ProfileController::class, 'perfil'])->name('perfil');

Route::get('/home', [HomeController::class, 'home'])->name('dashboard');


Route::prefix('profile')->group(function(){
   Route::get('/', [ProfileController::class, 'user_profile'])->name('user-profile');
   Route::post('/', [ProfileController::class, 'user_profile_update'])->name('user-profile-update');

    Route::view('credentials', 'users.user-credentials')->name('user-credentials');
    Route::post('credentials', [ProfileController::class, 'user_credentials_update'])->name('user-credentials-update');

    Route::view('photo', 'users.user-photo')->name('user-photo');
    Route::post('photo', [ProfileController::class,'user_photo_update'])->name('user-photo-update');
});

Route::prefix('mazza-admin')->group(function(){
    Route::get('/', [DashController::class, 'index'])->name('dash');
    
    Route::get('/addMentor', [DashController::class, 'addMentor'])->name('addMentor');
    Route::post('/storeMentor', [DashController::class, 'storeMentor'])->name('storeMentor');
    Route::get('/addModulo', [DashController::class, 'addModulo'])->name('addModulo');
    Route::get('/addPergunta', [DashController::class, 'addPergunta'])->name('addPergunta');
    Route::get('/addCategoria', [DashController::class, 'addCategoria'])->name('addCategoria');
    Route::post('/storeCategoria', [DashController::class, 'addCategoria'])->name('addCategoria');
 });

