<html>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Mazza</title>

<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->

<link rel="stylesheet" href="{{ asset('css/mystyle.css') }}">
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
<link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">

<title>Aulas</title>

<body>

    <div class="container area-header">

        <!-- INICIO DO NAVBAR -->
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="#">
                <h4>Tenha conhecimento de um Mazza</h4>
            </a>

            <div class="collapse navbar-collapse flex-row-reverse" id="navbarSupportedContent">
                <img src="{{ asset('img/mazza.png') }}" alt="logo" style="width:80px;">
            </div>
        </nav>
        <!-- FIM DO NAVBAR -->
    </div>

    <!-- div dos textos -->
    <div class="aulas-textos">
        <div class="row">
            <div class="col-md-2 div-botao">
                <button type="buttom" class="btn-primary botao" disabled>
                    <i class="fas fa-angle-left"></i>
                </button>
            </div>
            <div class="col-md-8 tabContent">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="individual-tab" data-toggle="tab" href="#individual" role="tab"
                            aria-controls="individual" aria-selected="true">INDIVIDUAL</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="empresa-tab" data-toggle="tab" href="#empresa" role="tab"
                            aria-controls="empresa" aria-selected="false">EMPRESA</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="individual" role="tabpanel"
                        aria-labelledby="individual-tab">
                        <form method="post" action="{{ route('storeSolicitarIndividual') }}">
                            @csrf
                            <div class="row m-2">
                                <div class="col-md-6">
                                    <input type="hidden" name="tipo" value="Individual">
                                    <div class="form-group">
                                        <label>Nome e Apelido</label>
                                        <input name="nome_completo" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Profissão/Função</label>
                                        <input name="profissao" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <input name="email" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Número de tefelone</label>
                                        <input name="telefone" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>País</label>
                                        <input name="pais" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Provincia</label>
                                        <input name="provincia" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Cidade</label>
                                        <input name="cidade" type="text" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Queira por favor Descrever o serviço que precisa</label>
                                        <textarea name="descricao" class="form-control" rows="22" cols="20" required></textarea>
                                    </div>
                                </div>
                                <hr class="hr">
                                <div class="col-md-12">
                                    <center>
                                        <button type="submit" class="btn-primary botaoConfirmar">Continuar</button>
                                    </center>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="empresa" role="tabpanel" aria-labelledby="empresa-tab">

                        <form method="post" action="{{ route('storeSolicitarEmpresa') }}">
                            @csrf
                            <div class="row m-2">
                                <div class="col-md-6">
                                    <input type="hidden" name="tipo" value="Empresa">
                                    <div class="form-group">
                                        <label>Nome de Registro</label>
                                        <input name="nome_completo" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Nuit</label>
                                        <input name="nuit" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <input name="email" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Endereço</label>
                                        <input name="endereco" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>País</label>
                                        <input name="pais" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Provincia</label>
                                        <input name="provincia" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Cidade</label>
                                        <input name="cidade" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Para quantas pessoas deseja o serviço?</label>
                                        <input name="nr_pessoa" type="number" class="form-control" min="1">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Queira por favor Descrever o serviço que precisa</label>
                                        <textarea name="descricao" class="form-control" rows="26" cols="20" required></textarea>
                                    </div>
                                </div>
                                <hr class="hr">
                                <div class="col-md-12">
                                    <center>
                                        <button type="submit" class="btn-primary botaoConfirmar">Continuar</button>
                                    </center>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-2 div-botao">
                <button type="submit" class="btn-primary botao">
                    <i class="fas fa-angle-right"></i>
                </button>
            </div>
        </div>
    </div>

    <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>

</body>

</html>