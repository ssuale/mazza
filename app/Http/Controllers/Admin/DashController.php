<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mentor;
use Image;

class DashController extends Controller
{
    



    public function index()
    {
        return view('admin.admin-index');
        
    }


    // Mentor Method
    public function addMentor(){
        return view('admin.addMentor');
    }


    public function storeMentor(Request $request){
        
        $save_url = "";



        if($request->file('brand_image')){
    	$brand_image = $request->file('brand_image');
    	$name_gen = hexdec(uniqid()).'.'.$brand_image->getClientOriginalExtension();
    	Image::make($brand_image)->resize(247,247)->save('upload/image/'.$name_gen);
    	$save_url = 'upload/image/'.$name_gen;
    }
	Mentor::Create([
        'nome'=> $request->nome,
        'descricao'=> $request->description,
        'image' => $save_url,
        'profisao'=> $request->profisao, 
        
    ]);

	    $notification = array(
			'message' => 'Mentor Creado com Sucesso',
			'alert-type' => 'success'
		);

		return redirect()->back()->with($notification);
    }





    public function addModulo(){
        return view('admin.addModulo');
    }

    public function addPergunta(){
        return view('admin.addPergunta');
    }

    public function addCategoria(){
        return view('admin.addCategoria');
    }
}
