<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aluno;
use App\Models\CardInfo;
use App\Models\PreModulo;
use App\Models\ModuloAssinados;
use App\Models\Subscricao;

class ProfileController extends Controller
{
    public function lesson(Course $course, $id)
    {
        if (!Enroll::whereUserId(auth()->user()->id)->whereCourseId($course->id)) {
            return abort(404, 'You have not purchased this course yet!');
        }
        $lesson = Lesson::find($id);
        $video = '';
        for ($i = strpos($lesson->video, 'v=') + 2; $i < strlen($lesson->video); $i++) {
            $video .= $lesson->video[$i];
        }

        if (!$course->lessons()->find($id)) {
            return abort(404);
        }

        $lessons = $course->lessons;

        return view('users.lesson', compact(['course', 'lesson', 'lessons', 'video']));
    }

    public function user_profile()
    {
        return view('users.user-profile');
    }

    public function user_profile_update(Request $request)
    {
        $user = auth()->user();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->save();

        return back();
    }

    public function user_credentials_update(Request $request)
    {
        if (!(Hash::check($request->get('current_password'), auth()->user()->password))) {
            // The passwords doesn't matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
        }

        if (strcmp($request->get('new_password'), $request->get('confirm_new_password')) != 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }

        $this->validate(request(), [
            'current_password' => 'required',
            'new_password' => 'required|string|min:6',
        ]);

        //Change Password
        $user = auth()->user();
        $user->email = $request->email;
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return redirect()->back()->with("success", "Password changed successfully");
    }

    public function user_photo_update(Request $request)
    {
        $originalImage = $request->file('user_image');
        $imageName = time() . $originalImage->getClientOriginalName();
        $originalImage = Image::make($originalImage);
        $originalPath = public_path() . '/images/';
        //dd($originalPath);
        $originalImage->save($originalPath . $imageName);

        $user = auth()->user();
        $user->photo = $imageName;
        $user->save();

        return back();
    }

    public function perfil(){
        $aluno = Aluno::where('user_id', auth()->user()->id)->first();
        $niveis = ModuloAssinados::where('aluno_id', $aluno->id)->get();
        $interesses = PreModulo::where('aluno_id', $aluno->id)->get();
        $card = CardInfo::where('aluno_id', $aluno->id)->first();
        $plano = Subscricao::where('aluno_id', $aluno->id)->first();
        
        $level = ["Iniciante", "Básico", "Intermédio", "Profissional", "Experiente", "Professor"];
        $count = [0, 0, 0, 0, 0, 0];
        $levelData = array();

        foreach ($niveis as $key => $value) {
            $levelData[] =  $value->modulos->nivel;
        }
        
        $max = $count[0];
        $k = 0;

        for($i = 0; $i < count($level); $i++)
        {
            for($j = 0; $j < count($levelData); $j++)
            {
                if($level[$i] == $levelData[$j])
                {
                    $count[$i]++;
                }
            }
        }
        for($i = 0; $i < count($level); $i++)
        {
            if($count[$i] > $max)
            {
                $k = $i;
                $max = $count[$i];
            }
        }
        $nivel = $level[$k];

        $size = 0; 
        $sizediff = 100; 
            
        if($nivel == "Iniciante"){
            $size = "width:0%";
            $sizediff = "width:100%"; 
        }else if($nivel == "Básico"){
            $size = "width:20%";
            $sizediff = "width:80%"; 
        }else if($nivel == "Intermédio"){
            $size = "width:40%";
            $sizediff = "width:60%"; 
        }else if($nivel == "Profissional"){
            $size = "width:60%";
            $sizediff = "width:40%"; 
        }else if($nivel == "Experiente"){
            $size = "width:80%";
            $sizediff = "width:20%"; 
        }else if($nivel == "Professor"){
            $size = "width:100%";
            $sizediff = "width:0%"; 
        }

        return view('perfil', compact('nivel', 'interesses', 'card', 'plano', 'size', 'sizediff'));
    }
}