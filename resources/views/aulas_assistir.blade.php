<html>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Mazza</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<link rel="stylesheet" href="{{ asset('css/mystyle.css') }}">
<link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">

<title>Aulas</title>

<body>

    <div class="container area-header">

        <!-- INICIO DO NAVBAR -->
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('img/mazza.png') }}" alt="logo" style="width:80px;">
            </a>
            <div class="hv"></div>
            <h2 style="margin: 5px;">Aceleradora<br>de Negócios</h2>

            <div class="collapse navbar-collapse flex-row-reverse" id="navbarSupportedContent">

            </div>
        </nav>
        <!-- FIM DO NAVBAR -->
    </div>

    <!-- div dos textos -->
    <div class="aulas-textos">
        <div class="container">
            <div class="row m-4">
                @foreach($aulas as $key => $aula)
                @if($key == 0)
                <h3 class="tituloAula">{{ $aula->nome }}</h3>
                <iframe width="100%" height="415" src="{{ $aula->link }}" title="{{ $aula->nome }}" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
                @endif
                @endforeach
            </div>
            <div class="row m-4">
                @foreach($aulas as $key => $aula)
                @if($key > 0 and $key < 5) <div class="col-md-3">
                    <iframe src="{{ $aula->link }}" frameborder="0" width="100%" height="50%"></iframe>
                    <h6 class="h6Cont">{{ $aula->nome }}</h6>
                    <p class="text-center">............................</p>
                    <p class="text-center">12345</p>
            </div>
            @endif
            @endforeach
        </div>
        <hr>
        <div class="container mt-5">
            <h6 class="h6Cont">Contactos:&nbsp;&nbsp;<i class="far fa-envelope"></i>&nbsp;info@believer.co.mz
                &nbsp;&nbsp;&nbsp;<i class="fas fa-phone-square-alt"></i>&nbsp;<i class="fab fa-whatsapp"></i>&nbsp;+258
                86 97 50000 &nbsp;&nbsp;&nbsp;<i class="fas fa-map-marker-alt"></i>&nbsp;Rua do Brado Africano, Nº46
            </h6>
        </div>
    </div>

    <!-- <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script> -->

</body>

</html>