<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitacaos', function (Blueprint $table) {
            $table->id();
            $table->enum('tipo', ['Individual', 'Empresa'])->default('Individual');
            $table->string('nome_completo');
            $table->integer('nuit')->nullable();
            $table->string('profissao')->nullable();
            $table->string('email');
            $table->string('telefone')->nullable();
            $table->string('pais');
            $table->string('provincia');
            $table->string('cidade');
            $table->string('endereco')->nullable();
            $table->integer('nr_pessoa')->nullable();
            $table->longText('descricao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitacaos');
    }
}
