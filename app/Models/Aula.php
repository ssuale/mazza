<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aula extends Model
{
    use HasFactory;

    protected $fillable = [
        'modulo_id',
        'nome',
        'position',
        'link'
    ];

    public function modulo(){
        return $this->belongsTo('App\Models\Modulo', 'modulo_id');
    }
}