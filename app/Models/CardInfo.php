<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardInfo extends Model
{
    use HasFactory;

    protected $fillable = [
        'numero',
        'rede',
        'nome',
        'apelido',
        'data_expira',
        'cvv',
        'aluno_id',
    ];

    public function aluno(){
        return $this->belongsTo('App\Models\Aluno', 'aluno_id');
    }
}
