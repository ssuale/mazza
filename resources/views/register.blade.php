<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Register</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css">
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400&display=swap" rel="stylesheet">



    <style>
    body {
        font-family: 'Century Gothic', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
    }

    label {
        color: #484646;
    }

    header {


        padding: 50px;
        color: #fff;

    }

    .form-section span {
        font-size: 1.5rem;
        color: #0a5586;
        font-weight: 500;
    }


    .form-section p {
        font-size: 0.9rem;
        color: #0a5586;
        font-weight: 300;
    }
    </style>
</head>

<body>

    <header style="background-image: url('/images/login.jpeg');">
        <div style=" float:right;margin-top: -35px">
            <h3>maZza</h3>
            <span style="color: #fff;
    font-weight: 550;">ACELERADORA
                DE
                NEGÓCIOS</span>
        </div>
    </header>

    <section style="padding-top:50px" class="form-section">
        <div class="container">
            <span>
                Seja muito bem-vindo(a) ao campo de pré-inscrição do <br>
                Mazza aceleradora de negócios e mentoria.
            </span>
            <br>
            <p>
                Em seguida faremos algumas questões ao Senhor(a) para apurar em que fase se encontra através do seu
                perfil. <br>
                Por favor dei-nos um pouco do seu tempo para preencher todas as informações.
            </p>


            <form>
                <div class="row">

                    <div class="col-md-6">


                        <div class="form-group">
                            <label>1. Nome e Apelido</label>
                            <input type="text" class="form-control">

                        </div>
                        <div class="form-group">
                            <label>2. O seu e-mail?</label>
                            <input type="text" class="form-control">

                        </div>

                        <div class="form-group">
                            <label>3. O seu número para chamadas?</label>
                            <input type="text" class="form-control">

                        </div>

                        <div class="form-group">
                            <label>4. O seu número para o WhatsApp Business?</label>
                            <input type="text" class="form-control">

                        </div>


                    </div>

                    <div class="col-md-6">


                        <div class="form-group">
                            <label>5. Qual é o link para acesso ao seu perfil no LinkedIn?</label>
                            <input type="text" class="form-control">

                        </div>
                        <div class="form-group">
                            <label>6. Qual é o nome da sua empresa?</label>
                            <input type="text" class="form-control">

                        </div>

                        <div class="form-group">
                            <label>7. Qual é a sua função?</label>

                            <div class="row">

                                <div class="col-6">

                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input">
                                        <label class="form-check-label">a) Presidente do conselho de administração
                                            (PCA).</label>
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input">
                                        <label class="form-check-label">b) Director executivo.</label>
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input">
                                        <label class="form-check-label">c) Director de administração e finanças.</label>
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input">
                                        <label class="form-check-label">d) Director de Marketing.</label>
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input">
                                        <label class="form-check-label">e) Consultor.</label>
                                    </div>
                                </div>




                                <div class="col-6">

                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input">
                                        <label class="form-check-label">f) IT.</label>
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input">
                                        <label class="form-check-label">g) Sócio ou fundador.</label>
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input">
                                        <label class="form-check-label">h) Técnico.</label>
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input">
                                        <label class="form-check-label">i) Recursos humanos.</label>
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input">
                                        <label class="form-check-label">j) Outro.</label>
                                    </div>
                                </div>
                            </div>

                        </div>




                    </div>

                </div>
                <center>
                    <button type="submit" class="btn btn-primary">Registar</button>
                </center>

            </form>
        </div>

        <br>


        <br>
    </section>
    <footer style="border-top: 2px solid #b7b2b2;padding:30px; margin-top:50px">
        <div class="container">

            <div class="row">

                <div class="col-sm-12 col-4">

                    <span>Contactos: info@believer.co.mz</span>
                </div>
                <div class="col-sm-12 col-4">

                    <span><i class="fa fa-phone" aria-hidden="true"></i> +258 86 97 50000</span>
                </div>

                <div class="col-sm-12 col-4">

                    <span>

                        <i class="fa fa-map-marker" aria-hidden="true"></i> Rua do Brado Africano, No46</span>


                </div>
            </div>
        </div>

    </footer>
    <script src="/js/jquery-3.6.0.min.js"></script>
    <!-- <script src="js/bootstrap.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
 <script src="js/bootstrap.bundle.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"
        integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous">
    </script>
</body>

</html>