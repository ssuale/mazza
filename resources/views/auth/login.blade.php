
<html>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Login</title>

<head>
<link rel="stylesheet" href="{{ asset('css/login.css') }}">
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>
<body>

<section id="login-section">
<div id="webForm">
<div class="container ">
    <div class="row">
        <div class="col-md-7">
            <div class="bemVindo">
                <h1 style="font-size: 64px; margin-top: 20px;">Seja muito <br/>Bem-Vindo(a)<br> à maZza</h1>
            </div>
        </div> 
      
        <div class="col-sm-12 col-md-5">
        <form  method="POST" action="{{ route('login') }}" id="form" >
            @csrf
            <div class="wapper-form">
                <div class="form-login">
                <div class="logo-form"><p></p></div>
                <div>
                    <center>
                        <span>
                        <a href="/">REGISTAR</a>
                        </span>
                    </center>
                </div>
                <div>
                <center>
                    <input type="email" name="email" placeholder="Email" class="input" required>
                </center>
            </div>
           
            <div>
                <center>
                    <input type="password" name="password" placeholder="Senha" class="input" required>
                </center>
            </div>
            <div class="register" style="margin-top:100px;">
            <center>
                <a href="javascript:void(0)" onclick="document.getElementById('form').submit()">
                                                <img class="signin-image" src="{{asset('images/assets/foward-login.png')}}" />
                                            </a>
                <center>
            </div>
                </div>
            </div>
            <div class="aceleradora">
            <center>
                <p>ACELERADORA DE NEGÓCIOS</p>
            </center>
            </form>
        </div>

        </div>
    </div>
</div>
</div>
<div id="mobileForm">
<div class="container ">
    <div class="row">
    <div class="col-md-7">
            <div class="bemVindo">
                <p>Seja muito bem vindo(a)</p>
            </div>
        </div>
        <form method="post" action="{{ route('step3') }}" >
	 	@csrf
        <div class="col-md-5">
            <div class="wapper-form">
                <div class="form-login">
                <div class="logo-form"><p></p></div>
                <div>
                <center>
                        <span>
                        <a href="/">ENTRAR</a>
                        </span>
                    </center>
                </div>
                <div>
                <center>
                    <input type="email" name="email" placeholder="Email" class="input" required>
                </center>
            </div>
           
            <div>
                <center>
                    <input type="password" name="password" placeholder="Senha" class="input" required>
                </center>
            </div>
            <div class="register" style="margin-top:100px;">
            <center>
                <button type="submit" class="login-button">
                    <i style="visibility: hidden;"> Login Mazza </i>
                </button>
                <center>
            </div>
                </div>
            </div>
            <div class="aceleradora">
            <center>
                <p>ACELERADORA DE NEGÓCIOS</p>
            </center>
        </div>
        </form>
        </div>
    </div>
</div>
</div>

</section>















<script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script>

</body>
</html>