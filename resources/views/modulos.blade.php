<html>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Mazza</title>
<link rel="stylesheet" href="{{ asset('css/mystyle.css') }}">
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
<link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">

<title>Aulas</title>

<body>

    <div class="container area-header">

        <!-- INICIO DO NAVBAR -->
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="#">
                <h4>Tenha conhecimento de um Mazza</h4>
            </a>

            <div class="collapse navbar-collapse flex-row-reverse" id="navbarSupportedContent">
                <img src="{{ asset('img/mazza.png') }}" alt="logo" style="width:80px;">
            </div>
        </nav>
        <!-- FIM DO NAVBAR -->
    </div>

    <!-- div dos textos -->
    <div class="aulas-textos">
        <!-- CHECKBOXS -->
        <div class="container checkpartes-alldivs">
            <div class="container-fluid">
                <form method="post" action="{{ route('sendRequest') }}">
                    @csrf
                    <input type="hidden" name="solicitacao_id" value="{{ $solicitacao->id }}" required>
                    <div class="row">
                        <div class="col-md-2 div-botao">
                            <button type="buttom" class="btn-primary botao" disabled>
                                <i class="fas fa-angle-left"></i>
                            </button>
                        </div>

                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6 checkpartes">
                                    <h5>Digital</h5>
                                    <p class="p6">Aqui os conteúdos são disponibilizados 100% online</p>
                                    @foreach($modulos as $key => $module)
                                    <?php if($module->categoria->category_name == "Digital"){ ?>
                                    <div class="form-check">
                                        <input class="form-check-input" name="modulo_id[]" type="checkbox"
                                            value="{{ $module->id }}" id="{{ $module->slug }}"required>
                                        <label class="form-check-label"
                                            for="{{ $module->slug }}">{{ $module->nome }}</label>
                                    </div>
                                    <?php } ?>
                                    @endforeach
                                </div>
                                <div class="col-md-6 checkpartes">
                                    <h5>Presencial</h5>
                                    <p class="p6">&nbsp;</p>
                                    @foreach($modulos as $key => $module)
                                    <?php if($module->categoria->category_name == "Presencial"){ ?>
                                    <div class="form-check">
                                        <input class="form-check-input" name="modulo_id[]" type="checkbox"
                                            value="{{ $module->id }}" id="{{ $module->slug }}" required>
                                        <label class="form-check-label"
                                            for="{{ $module->slug }}">{{ $module->nome }}</label>
                                    </div>
                                    <?php } ?>
                                    @endforeach
                                </div>
                                <hr class="hr">
                                <div class="col-md-12">
                                    <center>
                                        <button type="submit" class="btn-primary botaoConfirmar">Enviar</button>
                                    </center>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 div-botao">
                            <button type="submit" class="btn-primary botao">
                                <i class="fas fa-angle-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END CHECKBOXS -->
    </div>

    <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script>

</body>

</html>