<html>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Mazza</title>
<link rel="stylesheet" href="{{ asset('css/mystyle.css') }}">
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
<link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">

<title>Aulas</title>

<body>

    <div class="container area-header">

        <!-- INICIO DO NAVBAR -->
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="#">
                <h4>Tenha conhecimento de um Mazza</h4>
            </a>

            <div class="collapse navbar-collapse flex-row-reverse" id="navbarSupportedContent">
                <img src="{{ asset('img/mazza.png') }}" alt="logo" style="width:80px;">
            </div>
        </nav>
        <!-- FIM DO NAVBAR -->
    </div>

    <!-- div dos textos -->
    <div class="aulas-textos">
        <div class="container">
            <p class="p1">Concluiu com sucesso o passo #2</p>
            <p class="p2">Escolha suas aulas</p>
            <hr class="hr">
        </div>
        <!--fim da div dos textos -->

        <!-- CHECKBOXS -->
        <div class="container checkpartes-alldivs">
            <div class="container-fluid">
                <form method="post" action="{{ route('step5') }}" id="form">
                    @csrf
                    <input type="hidden" name="aluno_id" value="{{ $student->id }}" required>
                    <div class="row">
                        <div class="col-md-2 div-botao">
                            <button type="buttom" class="btn-primary botao" disabled>
                                <i class="fas fa-angle-left"></i>
                            </button>
                        </div>

                        <?php $t = count($modulos); $d = $t / 2; ?>
                        <div class="col-md-4 checkpartes">
                            @foreach($modulos as $key => $module)
                            <?php if($key < $d){ ?>
                            <div class="form-check">
                                <input class="form-check-input" name="modulo_id[]" type="checkbox"
                                    value="{{ $module->id }}" id="{{ $module->slug }}" required>
                                <label class="form-check-label" for="{{ $module->slug }}">{{ $module->nome }}</label>
                            </div>
                            <?php } ?>
                            @endforeach
                        </div>
                        <div class="col-md-4 checkpartes">
                            @foreach($modulos as $key => $module)
                            <?php if($key >= $d){ ?>
                            <div class="form-check">
                                <input class="form-check-input" name="modulo_id[]" type="checkbox"
                                    value="{{ $module->id }}" id="{{ $module->slug }}" required> 
                                <label class="form-check-label" for="{{ $module->slug }}">{{ $module->nome }}</label>
                            </div>
                            <?php } ?>
                            @endforeach
                        </div>

                        <div class="col-md-2 div-botao">
                            <button type="submit" class="btn-primary botao">
                                <i class="fas fa-angle-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END CHECKBOXS -->
    </div>

    <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script>

</body>

</html>