<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solicitacao extends Model
{
    use HasFactory;

    protected $fillable = [
        'tipo',
        'nome_completo',
        'nuit',
        'profissao',
        'email',
        'telefone',
        'pais',
        'provincia',
        'cidade',
        'endereco',
        'nr_pessoa',
        'descricao'
    ];
}
