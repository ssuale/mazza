<?php

namespace Database\Factories;

use App\Models\Subscricao;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubscricaoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Subscricao::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
