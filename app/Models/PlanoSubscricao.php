<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanoSubscricao extends Model
{
    use HasFactory;

    protected $fillable = [
        'periodicidade',
        'taxa'
    ];

    public function subscricao(){
        return $this->hasMany('App\Models\Subscricao');
    }
}
