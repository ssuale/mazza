<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Mazza</title>
        <!-- <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/main.css"> -->

        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mazza.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <!-- Fonts -->
       
        <!-- Styles -->
        <style>
          
        </style>

        
    </head>
    <body>


    <header>
     <div class="container desk-menu">
     
     <div class="row">
     <div class="col-sm-8 col-md-10 col-Logo" > 
     <div class="header-img">
  
     <img src="{{ asset('images/assets/mazza.png') }}" class="imgLogo"> 
     </div>
     <div class="header-text">
     <h2>Aceleradora de</h2>
     <h2>Negócios</h2>
     </div>

     
     </div>
    
     <div class="col-sm-4 col-md-2 col-Menu">

     <div class="menu-nav">
 

  
        <a class="nav-link " href="#" id="navbarScrollingDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-bars" aria-hidden="true"></i>
        </a>
        <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
          <li><a class="dropdown-item" href="/">Home</a></li>
          <li><a class="dropdown-item" href="#">Cursos</a></li>
          <li><a class="dropdown-item" href="#">Meus Cursos</a></li>
          <li><a class="dropdown-item" href="/profile">Profile</a></li>
          <li><a class="dropdown-item" href="/login">Login</a></li>
          <li><a class="dropdown-item" href="#">Logout</a></li>
        
        </ul>
    
     </div>
     
     </div>
     </div>
     </div>
  <div class="mobileMenu">

  <div class="horizontal-align two-items">
  <div class="item">
  <div class="header-img">
  
  <img src="{{ asset('images/assets/mazza.png') }}" class="imgLogo"> 
  </div>
</div>

<div class="item">
<div class="menu-nav">
 

  
 <a class="nav-link " href="#" id="navbarScrollingDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
 <i class="fa fa-bars" aria-hidden="true"></i>
 </a>
 <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
   <li><a class="dropdown-item" href="/">Home</a></li>
   <li><a class="dropdown-item" href="#">Cursos</a></li>
   <li><a class="dropdown-item" href="#">Meus Cursos</a></li>
   <li><a class="dropdown-item" href="/profile">Profile</a></li>
   <li><a class="dropdown-item" href="/login">Login</a></li>
   <li><a class="dropdown-item" href="#">Logout</a></li>
 
 </ul>

</div>
</div>
</div>
  </div>
    </header>
    @yield('admin')
   
   <footer style="border-top: 2px solid #b7b2b2;padding:30px; margin-top:50px">
   <div class="container">
   
  <div class="row">
   
  <div class=" col-4">
   
   <span>Contactos: info@believer.co.mz</span>
   </div>
   <div class=" col-4">
   
   <span><i class="fa fa-phone" aria-hidden="true"></i> +258 86 97 50000</span>
   </div>

   <div class=" col-4">
   
   <span>
   
   <i class="fa fa-map-marker" aria-hidden="true"></i> Rua do Brado Africano, No46</span>

   
   </div>
  </div>
   </div>
   
   </footer>
<!-- <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
 <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
 <script src="/js/bootstrap.bundle.min.js"></script> -->
 <!-- -->
     <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script> 
  



    </body>
</html>
