<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
        'slug',
        'formacao',
        'mentor_id',
        'price',
        'image',
        'nivel',
        'categoria_id'
    ];

    public function aulas(){
        return $this->hasMany('App\Models\Aula');
    }

    public function categoria(){
        return $this->belongsTo('App\Models\Categoria', 'categoria_id');
    }

    public function mentor(){
        return $this->belongsTo('App\Models\Mentor', 'mentor_id');
    }

    public function premodulos(){
        return $this->hasMany('App\Models\PreModulo');
    }

    public function moduloassinados(){
        return $this->hasMany('App\Models\ModuloAssinados');
    }
}
