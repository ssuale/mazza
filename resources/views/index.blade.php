<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mazza</title>
    <link rel="stylesheet" href="{{ asset('css/mystyle.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>

<body>
    <div class="container area-header">

        <!-- INICIO DO NAVBAR -->
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="#">
                <img src="img/mazza.png" alt="logo" style="width:80px;">
            </a>

            <div class="collapse navbar-collapse flex-row-reverse" id="navbarSupportedContent">
                <form class="form-inline my-2 my-lg-0">
                    <!--class="rounded-pill" para estar com border-radius-->
                    <a href="{{ route('login') }}" class="btn-entrar"><h5 class="m-2">Entrar</h5></a>
                    <a href="{{ route('login') }}"><img class="img-entrar" src="./img/foward-login.png" alt=""></a>
                </form>
            </div>
        </nav>
        <!-- FIM DO NAVBAR -->
    </div>

    <!-- div dos textos -->
    <div class="container area-textos">
        <h2>Conhecimento ilimitado, <br>mentorias, lives e muito mais…</h2>
        <p>Entregamos as ferramentas necessárias <br>para que você alcance seu objectivo.</p>
    </div>
    <!--fim da div dos textos -->


    <!-- formulario de email -->

    <form method="get" action="{{ route('step2') }}">
        <div class="container area-email">
            <p>Está preparado para ser um Mazza? Inseria seu e-mail para criar uma conta</p>
            <div class="input-group mb-3">
                <input type="email" name="email" class="form-control">
                <div class="input-group-append">
                    <button class="btn" type="submit">REGISTAR</button>
                </div>
            </div>
        </div>
    </form>
    <!-- fim do formulario de email -->

    <div class="area-footer">
        <p>ACELERADORA DE NEGÓCIOS</p>
    </div>


    <!-- Perguntas Frequentes -->
    <div class="container collapse-accordion">

        <div class="area-textos">
          <h1>Perguntas Frequentes</h1>
        </div>

        @foreach($perguntas as $key => $question)
        <button class="accordion btn-area-textos">{{ $question->title }}</button>
        <div class="panel">
          <p class="text-white text-center">{{ $question->description }}</p>
        </div>
        @endforeach

    </div>
    <!-- Perguntas Frequentes -->


    <script>
    // Funcionalidades do Accordion / Collapse

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }

    // Fim das Funcionalidades do Accordion / Collapse
    </script>
</body>

</html>