@extends('admin.admin')
@section('admin')

<div class="container">
<div class="row">
<div class="col-md-12" style="padding: 20px;">
<div>

<h3>Lista de Mentores</h3>
<a href="{{ route ('addMentor')}}"  class="btn btn-success" style="float:right;"> Adicionar Mentor</a>

</div>
</div></div>

<div class="row">
<div class="col-md-12">
<table id="table_id" class="display">
    <thead>
        <tr>
            <th>Column 1</th>
            <th>Column 2</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Row 1 Data 1</td>
            <td>Row 1 Data 2</td>
        </tr>
        <tr>
            <td>Row 2 Data 1</td>
            <td>Row 2 Data 2</td>
        </tr>
    </tbody>
</table>
</div>
</div>
</div>
@endsection